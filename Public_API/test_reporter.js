// test_reporter.js
const Mocha = require('mocha');
const AllureReporter = require('mocha-allure-reporter');

const mocha = new Mocha({
  reporter: AllureReporter,
});

mocha.addFile('./test_api_mocha.js');

mocha.run();
