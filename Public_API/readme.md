# Running Automated Tests Locally
## Prerequisites
Make sure that you have the latest version of Node.js installed on your machine.
Clone the repository to your local machine.

## Running the API Test
Open a terminal window and navigate to the project directory.
Run npm install to install all the dependencies.
Run npm test to execute the test script.

## Running the UI Test
Open a terminal window and navigate to the project directory.
Run npm install to install all the dependencies.
Run npm run test:ui to execute the test script.

## Generating Test Reports
After running the test scripts, open the allure-report directory in the project root.
Double-click the index.html file to view the generated test report in the browser.