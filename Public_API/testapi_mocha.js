// test_api_mocha.js

const chai = require('chai');
const chaiHttp = require('chai-http');

const expect = chai.expect;

chai.use(chaiHttp);

describe('API Test', function() {
  it('should find all objects with property "Category: Authentication & Authorization"', function(done) {
    this.timeout(5000)
    chai.request('https://api.publicapis.org')
      .get('/entries')
      .end(function(err, res) {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        
        const authenticationObjs = res.body.entries.filter(obj => obj.Category === 'Authentication & Authorization');
        expect(authenticationObjs).to.have.lengthOf(7); // change this number to match the actual number of objects with this category
        
        // assert that each object in the filtered array has the expected category
        authenticationObjs.forEach(obj => {
          expect(obj.Category).to.equal('Authentication & Authorization');
        });
        
        console.log(authenticationObjs); // print found objects to console
        
        done();
      });
  });
});