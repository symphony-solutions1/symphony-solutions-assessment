// Get the data
const BASE_URL = "https://api.publicapis.org"

const getRawData = async () => {
  const response = await fetch(BASE_URL + "/entries");
  return response.json();
}

// Filter the data
const test_api = async () => {
  const records = await getRawData()

  const filteredData = await records.entries.filter((record) => record.Category == "Authentication & Authorization");
  
  // Print the response
  console.log("Filtered Records Count >>> ", filteredData.length);
  console.log("Filtered Records Results >>> ", filteredData);
}

test_api();