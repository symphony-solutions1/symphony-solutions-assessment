/// <reference types="../cypress" />
describe('AQA assesment', () => {
  
  let productNames;  // Declare a variable to store the product names array

  beforeEach(() => {
    cy.visit('https://www.saucedemo.com')
    cy.get('[data-test="username"]').type('standard_user')
    cy.get('[data-test="password"]').type('secret_sauce')
    cy.get('[data-test="login-button"]').click()

    // Get the Elements containing the Product title
    cy.get('div.inventory_item_name')
      // Extract the text from the elements and store in the productNames array
      .then($items => {
        productNames = $items.map((index, html) => Cypress.$(html).text()).get();
      })
  })

  it('Sort A to Z', () => {
    cy.get('span > select').select('az')
    // Check that the strings in the arrays match the array sorted from A to Z
    cy.get('div.inventory_item_name')
      .then($items => {
        const ProductList = $items.map((index, html) => Cypress.$(html).text()).get();
        expect(ProductList).to.deep.eq(productNames.sort());
       // check the console to confirm the array
        console.log(productNames) 
      })
  })

  it('Sort Z to A', () => {
    cy.get('span > select').select('za')
    // Check that the strings in the arrays match the array sorted from Z to A
    cy.get('div.inventory_item_name')
      .then($items => {
        const ProductList = $items.map((index, html) => Cypress.$(html).text()).get();
        expect(ProductList).to.deep.eq(productNames.sort((a, b) => b.localeCompare(a)));
        
        console.log(productNames) 
      })
  })
})

