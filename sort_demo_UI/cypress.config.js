const { defineConfig } = require("cypress");

module.exports = defineConfig({
  watchForFileChanges : false,
  defaultCommandTimeout : 30000,
  viewportWidth : 1280,
  viewportHeight : 720,
  reporter: 'cypress-mochawesome-reporter',
  reporterOptions: {
    charts: true,
    overwrite: false,
    code: true,
    reportPageTitle: 'Sauce_demo',
    reportFilename: "[status]_[datetime]-[name]-report",
    timestamp: "longDate",
    embeddedScreenshots: true,
    inlineAssets: true,
    saveAllAttempts: false,
    embeddedScreenshots: true,

  },
  e2e: {
    setupNodeEvents(on, config) {
      require('cypress-mochawesome-reporter/plugin')(on);

    },
    chromeWebSecurity: false,
  },
});