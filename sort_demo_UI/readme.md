## AQA assessment test script using Cypress
This test script was written in JavaScript using the Cypress testing framework to perform automated testing on the website https://www.saucedemo.com.

## Purpose
The purpose of this test script is to verify that the sorting functionality for the products on the website works correctly. Specifically, it checks that when sorting by A to Z, the products are listed in alphabetical order from A to Z, and when sorting by Z to A, the products are listed in alphabetical order from Z to A.

## Prerequisites
To run this test script, you must have the following installed on your system:
•	Node.js
•	Cypress

## Running the Test Script
To run the test script:
1.	Clone this repository and navigate to the directory in the terminal.
2.	Install the dependencies by running npm install.
3.	To open the Cypress Test Runner, run npx cypress open.
4.	Select chrome as Test IDE.
5.	Click on the sort_test.spec.js file to run the tests.
6.	The tests should now run in a new window.

## Test Cases
This test script contains two test cases:
1.	Sort A to Z - this test case verifies that the products are listed in alphabetical order from A to Z when the user selects the "A to Z" sorting option.
2.	Sort Z to A - this test case verifies that the products are listed in alphabetical order from Z to A when the user selects the "Z to A" sorting option.

## Test Steps
Both test cases contain the following steps:
1.	Navigate to https://www.saucedemo.com.
2.	Enter a valid username and password and click the login button.
3.	Select the desired sorting option (A to Z or Z to A).
4.	Get the elements containing the product titles.
5.	Extract the text from the elements and return the text in an array and index it.
6.	Verify that the array of product titles matches the expected order (either A to Z or Z to A).

## Test Results
If the test script runs successfully, it should output a passing result for each of the two test cases. If a test case fails, the test script should output a detailed error message indicating which step of the test case failed.

## Test Environment
This test script was developed and tested using the following environment:
•	macOS Monterey 12.6
•	Node.js 16.14.2
•	Cypress 12.10.0
•	Chrome Browser version 112
Note: The test script may run differently in other environments or with different versions of the software used.

